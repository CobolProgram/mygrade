       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READ-MY-GRADE.
       AUTHOR. MMODPOWW.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT MYGRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION   IS LINE  SEQUENTIAL.
           SELECT AVG-FILE ASSIGN   TO "avg.txt"
              ORGANIZATION   IS LINE  SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  MYGRADE-FILE.
       01  MYGRADE-DETIALS.
           88 END-OF-MYGRADE-FILE   VALUE  HIGH-VALUE.
           05 COURSE.
              10 COURSE-ID PIC X(6).
              10 COURSE-NAME PIC X(50).
           05 COURSE-CREDIT.
              10 CREDIT   PIC 9.
              10 GRADE    PIC X(2).
       FD  AVG-FILE.
       01  AVG-DETIAL.
           05 AVG-GRADE      PIC 9V9(3).
           05 AVG-SCI-GRADE  PIC 9V9(3).
           05 AVG-CS-GRADE   PIC 9V9(3).
       WORKING-STORAGE SECTION.
       01 NUM-GRADE      PIC 9(3)V9(3).
       01 NUM-CREDIT     PIC 9(3)V9(3). 
       01 SCI-NUM        PIC 9(3)V9(3).
       01 SCI-CREDIT     PIC 9(3)V9(3).
       01 CS-NUM         PIC 9(3)V9(3).
       01 CS-CREDIT      PIC 9(3)V9(3).
       01 GRADENUM       PIC 9(3)V9(3).
       01 SUB1           PIC X.
       01 SUB2           PIC X(2).
       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT MYGRADE-FILE 
           OPEN  OUTPUT  AVG-FILE 
           PERFORM  UNTIL END-OF-MYGRADE-FILE 
              READ  MYGRADE-FILE 
                 AT END SET END-OF-MYGRADE-FILE TO TRUE
              END-READ 
              IF NOT END-OF-MYGRADE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT
                 MOVE COURSE-ID TO SUB1 
                 MOVE COURSE-ID TO SUB2
                 IF SUB1 EQUAL 3 THEN
                    PERFORM 002-PROCESS THRU 002-EXIT
                 END-IF
                 IF SUB2 EQUAL  31 THEN
                    DISPLAY "SUB2 : " SUB2
                    PERFORM  003-PROCESS THRU 003-EXIT
                 END-IF
              END-IF 
              COMPUTE  AVG-GRADE = NUM-GRADE / NUM-CREDIT 
              DISPLAY "AVG " AVG-GRADE
              COMPUTE  AVG-SCI-GRADE = SCI-NUM / SCI-CREDIT 
              DISPLAY "AVG-SCI " AVG-SCI-GRADE
              COMPUTE  AVG-CS-GRADE = CS-NUM / CS-CREDIT 
              DISPLAY "AVG-CS " AVG-CS-GRADE
           END-PERFORM
           WRITE AVG-DETIAL
           CLOSE MYGRADE-FILE 
           CLOSE AVG-FILE
           GOBACK 
           .
       001-PROCESS.          
           EVALUATE GRADE 
              WHEN "A" MOVE 4 TO GRADENUM
              WHEN "B+" MOVE 3.5 TO GRADENUM
              WHEN "B" MOVE 3 TO GRADENUM
              WHEN "C+" MOVE 2.5 TO GRADENUM
              WHEN "C" MOVE 2 TO GRADENUM
              WHEN "D+" MOVE 1.5 TO GRADENUM
              WHEN "D" MOVE 1 TO GRADENUM
           END-EVALUATE
           COMPUTE  NUM-CREDIT = NUM-CREDIT + CREDIT
           COMPUTE  NUM-GRADE = NUM-GRADE + (GRADENUM * CREDIT)               
           .
       001-EXIT.
           EXIT.

       002-PROCESS.          
           EVALUATE GRADE 
              WHEN "A" MOVE 4 TO GRADENUM
              WHEN "B+" MOVE 3.5 TO GRADENUM
              WHEN "B" MOVE 3 TO GRADENUM
              WHEN "C+" MOVE 2.5 TO GRADENUM
              WHEN "C" MOVE 2 TO GRADENUM
              WHEN "D+" MOVE 1.5 TO GRADENUM
              WHEN "D" MOVE 1 TO GRADENUM
           END-EVALUATE
           COMPUTE  SCI-CREDIT = SCI-CREDIT + CREDIT
           COMPUTE  SCI-NUM = SCI-NUM + (GRADENUM * CREDIT)               
           .
       002-EXIT.
           EXIT.
       
       003-PROCESS.          
           EVALUATE GRADE 
              WHEN "A" MOVE 4 TO GRADENUM
              WHEN "B+" MOVE 3.5 TO GRADENUM
              WHEN "B" MOVE 3 TO GRADENUM
              WHEN "C+" MOVE 2.5 TO GRADENUM
              WHEN "C" MOVE 2 TO GRADENUM
              WHEN "D+" MOVE 1.5 TO GRADENUM
              WHEN "D" MOVE 1 TO GRADENUM
           END-EVALUATE
           COMPUTE  CS-CREDIT = CS-CREDIT + CREDIT
           COMPUTE  CS-NUM = CS-NUM + (GRADENUM * CREDIT)               
           .
       003-EXIT.
           EXIT.