       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-MY-GRADE.
       AUTHOR. MMODPOWW.
       
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT MYGRADE-FILE   ASSIGN TO   "mygrade.txt"
              ORGANIZATION   IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION.
       FD  MYGRADE-FILE.
       01  MYGRADE-DETIALS.
           88 END-OF-MYGRADE-FILE   VALUE  HIGH-VALUE.
           05 COURSE.
              10 COURSE-ID PIC X(6).
              10 COURSE-NAME PIC X(50).
           05 COURSE-CREDIT.
              10 CREDIT   PIC 9.
              10 GRADE    PIC X(2).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN  OUTPUT MYGRADE-FILE
           *>/202101Information Services and Study Fundamentals 2B+
           MOVE  "202101"  TO COURSE-ID
           MOVE "Information Services and Study Fundamentals" 
                TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE MYGRADE-DETIALS
           
           *>/212101English I                                         3C
           MOVE  "212101"  TO COURSE-ID
           MOVE "English I" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS
           
           *>/302111Calculus and Analytic Geometry I                  3C
           MOVE  "302111"  TO COURSE-ID
           MOVE "Calculus and Analytic Geometry I" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS
           
           *>/303101Chemistry I                                       3C+
           MOVE  "303101"  TO COURSE-ID
           MOVE "Chemistry I" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS
           
           *>/303102Chemistry Laboratory I                            1A
           MOVE  "303102"  TO COURSE-ID
           MOVE "Chemistry Laboratory I" TO COURSE-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/306101General Biology I                                 3C+
           MOVE  "306101"  TO COURSE-ID
           MOVE "General Biology I" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/306102General Biology Laboratory I                      1C+
           MOVE  "306102"  TO COURSE-ID
           MOVE "General Biology Laboratory I" TO COURSE-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/308101Introductory Physics I                            3C
           MOVE  "308101"  TO COURSE-ID
           MOVE "Introductory Physics I " TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/308102Introductory Physics Laboratory I                 1B
           MOVE  "308102"  TO COURSE-ID
           MOVE "Introductory Physics Laboratory I" TO COURSE-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/212102English II                                        3B
           MOVE  "212102"  TO COURSE-ID
           MOVE "English II" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/302112Calculus and Analytic Geometry II                 3B
           MOVE  "302112"  TO COURSE-ID
           MOVE "Calculus and Analytic Geometry II" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/303103Chemistry II                                      3C
           MOVE  "303103"  TO COURSE-ID
           MOVE "Chemistry II" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/303104Chemistry Laboratory II                           1B+
           MOVE  "303104"  TO COURSE-ID
           MOVE "Chemistry Laboratory II" TO COURSE-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/306103General Biology II                                3C
           MOVE  "306103"  TO COURSE-ID
           MOVE "General Biology II" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/306104General Biology Laboratory II                     1C+
           MOVE  "306104"  TO COURSE-ID
           MOVE "General Biology Laboratory II" TO COURSE-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/308103Introductory Physics II                           3C
           MOVE  "308103"  TO COURSE-ID
           MOVE "Introductory Physics II" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/308104Introductory Physics Laboratory II                1C+
           MOVE  "308104"  TO COURSE-ID
           MOVE "Introductory Physics Laboratory II" TO COURSE-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310201Computer and Data Processing                      3A
           MOVE  "310201"  TO COURSE-ID
           MOVE "Computer and Data Processing" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/203100Man and Civilization                              2C
           MOVE  "203100"  TO COURSE-ID
           MOVE "Man and Civilization" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/212301Reading Laboratory I                              2C+
           MOVE  "212301"  TO COURSE-ID
           MOVE "Reading Laboratory I" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/226100Man and Economy                                   2C
           MOVE  "226100"  TO COURSE-ID
           MOVE "Man and Economy" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/302213Calculus and Analytic Geometry III                2D+
           MOVE  "302213"  TO COURSE-ID
           MOVE "Calculus and Analytic Geometry III" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "D+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/302323Linear Algebra I                                  3D
           MOVE  "302323"  TO COURSE-ID
           MOVE "Linear Algebra I" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "D"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310211Computer Science I                                3C+
           MOVE  "310211"  TO COURSE-ID
           MOVE "Computer Science I" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310212Discrete Mathematics                              3C
           MOVE  "310212"  TO COURSE-ID
           MOVE "Discrete Mathematics" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/312201Elementary Statistics                             3B
           MOVE  "312201"  TO COURSE-ID
           MOVE "Elementary Statistics" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/208101Thai I                                            2C+
           MOVE  "208101"  TO COURSE-ID
           MOVE "Thai I" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/212302Reading Laboratory II                             2C
           MOVE  "212302"  TO COURSE-ID
           MOVE "Reading Laboratory II" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/224101Man and Politics                                  2B
           MOVE  "224101"  TO COURSE-ID
           MOVE "Man and Politics" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/225100Man, Society and Culture                          2B
           MOVE  "225100"  TO COURSE-ID
           MOVE "Man, Society and Culture" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/302315Ordinary Differential Equations                   3C
           MOVE  "302315"  TO COURSE-ID
           MOVE "Ordinary Differential Equations" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310213Computer Science II                               3B+
           MOVE  "310213"  TO COURSE-ID
           MOVE "Computer Science II" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310231Computer Oganization and Assembly Language        3A
           MOVE  "310231"  TO COURSE-ID
           MOVE "Computer Oganization and Assembly Language" 
           TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310341Data Structure and Algorithms                     3B
           MOVE  "310341"  TO COURSE-ID
           MOVE "Data Structure and Algorithms" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/441101Wellness Development                              2B
           MOVE  "441101"  TO COURSE-ID
           MOVE "Wellness Development" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/601101Man and Aesthetics                                2B
           MOVE  "601101"  TO COURSE-ID
           MOVE "Man and Aesthetics" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/226111Introduction to Economics                         2C
           MOVE  "226111"  TO COURSE-ID
           MOVE "Introduction to Economics" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/230416Production Management                             3D
           MOVE  "230416"  TO COURSE-ID
           MOVE "Production Management" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "D"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/308371Introduction to Electronics                       3C+
           MOVE  "308371"  TO COURSE-ID
           MOVE "Introduction to Electronics" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310332Computer Architecture                             3B
           MOVE  "310332"  TO COURSE-ID
           MOVE "Computer Architecture" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310444File Processing                                   3A
           MOVE  "310444"  TO COURSE-ID
           MOVE "File Processing" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310482Computer Graphic                                  3B
           MOVE  "310482"  TO COURSE-ID
           MOVE "Computer Graphic" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/301301Quality Management                                1D
           MOVE  "301301"  TO COURSE-ID
           MOVE "Quality Management" TO COURSE-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "D"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310327Principles of Programming Languages               3A
           MOVE  "310327"  TO COURSE-ID
           MOVE "Principles of Programming Languages" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310342Database Design                                   3B
           MOVE  "310342"  TO COURSE-ID
           MOVE "Database Design" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310414Software Engineering                              3B
           MOVE  "310414"  TO COURSE-ID
           MOVE "Software Engineering" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310453Operating System                                  3A
           MOVE  "310453"  TO COURSE-ID
           MOVE "Operating System" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310461Data Communication                                3C+
           MOVE  "310461"  TO COURSE-ID
           MOVE "Data Communication" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/441113Basketball I                                      1A
           MOVE  "441113"  TO COURSE-ID
           MOVE "Basketball I" TO COURSE-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310452Compiler Construction                             3B+
           MOVE  "310452"  TO COURSE-ID
           MOVE "Compiler Construction" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>//310462Computer Networks                                 3A
           MOVE  "310462"  TO COURSE-ID
           MOVE "Computer Networks" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310481Systems Analysis and Design                       3C
           MOVE  "310481"  TO COURSE-ID
           MOVE "Systems Analysis and Design" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310484Introduction to Artificial Intelligence           3C
           MOVE  "310484"  TO COURSE-ID
           MOVE "Introduction to Artificial Intelligence" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310485Office Automation Management                      3C+
           MOVE  "310485"  TO COURSE-ID
           MOVE "Office Automation Management" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "C+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310492Computer Seminar                                  1B+
           MOVE  "310492"  TO COURSE-ID
           MOVE "Computer Seminar" TO COURSE-NAME
           MOVE  "1"   TO CREDIT
           MOVE  "B+"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310417Selected Topics                                   3B
           MOVE  "310417"  TO COURSE-ID
           MOVE "Selected Topics" TO COURSE-NAME
           MOVE  "3"   TO CREDIT
           MOVE  "B"  TO GRADE
           WRITE MYGRADE-DETIALS

           *>/310491Projects in Computer Science                      2A
           MOVE  "310491"  TO COURSE-ID
           MOVE "Projects in Computer Science" TO COURSE-NAME
           MOVE  "2"   TO CREDIT
           MOVE  "A"  TO GRADE
           WRITE MYGRADE-DETIALS
           CLOSE MYGRADE-FILE
           GOBACK 
           .
